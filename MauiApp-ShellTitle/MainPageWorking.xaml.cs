﻿namespace MauiApp_ShellTitle;

public partial class MainPageWorking : ContentPage
{
	private int _onAppearingCounter = 0;

	public MainPageWorking()
	{
		InitializeComponent();
		Title = "Set on Constructor";
	}

	protected override void OnAppearing()
	{
		base.OnAppearing();

		string title = GetTitle();

		Title = title;
		_onAppearingCounter++;

		lblCounter.Text = $"OnAppearing Count: {_onAppearingCounter}";
	}

	private string GetTitle()
	{
		return "Set on OnAppearing (Working)";
	}
}
