﻿namespace MauiApp_ShellTitle;

public partial class MainPage : ContentPage
{
	private int _onAppearingCounter = 0;

	public MainPage()
	{
		InitializeComponent();
		Title = "Set on Constructor";
	}

	protected override async void OnAppearing()
	{
		base.OnAppearing();

		string title = await GetTitle();

		Title = title;

		_onAppearingCounter++;

		lblCounter.Text = $"OnAppearing Count: {_onAppearingCounter}";
	}

	private async Task<string> GetTitle()
	{
		await Task.Delay(1000);
		return "Set on OnAppearing (Not working on first 'OnAppearing')";
	}
}
